FROM openjdk:8-jdk-alpine
RUN addgroup -S jenkins && adduser -S jenkins -G jenkins
USER jenkins:jenkins
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} spring-ci.jar
ENTRYPOINT ["java","-jar","/spring-ci.jar"]
