package com.example.cispringdemo.utils;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class SystemProp {

	public Map<String, String> printInfo() {
		RuntimeMXBean runtimeBean = ManagementFactory.getRuntimeMXBean();

		Map<String, String> systemProperties = runtimeBean.getSystemProperties();
		return systemProperties;
	}

}
