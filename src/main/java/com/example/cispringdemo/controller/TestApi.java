package com.example.cispringdemo.controller;

import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.cispringdemo.utils.SystemProp;

@RestController
public class TestApi {

	@Autowired
	SystemProp sysProp;
	
	@GetMapping("/")
	public  Map<String, String> sendText() {
		return sysProp.printInfo();
	}
	
	@GetMapping("/version")
	public  String sendVersion() {
		return "Version 6.5";
	}


}
