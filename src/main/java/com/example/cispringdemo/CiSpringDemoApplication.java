package com.example.cispringdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiSpringDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CiSpringDemoApplication.class, args);
	}

}
